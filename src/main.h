#ifndef MAIN_H
#define MAIN_H

#include <QStandardPaths>
#include <QDesktopWidget>
#include <QSharedMemory>
#include <QStorageInfo>
#include <QLocalSocket>
#include <QLocalServer>
#include <QInputDialog>
#include <QMessageBox>
#include <QFileDialog>
#include <QCloseEvent>
#include <QTranslator>
#include <QSettings>
#include <QFileInfo>
#include <QShortcut>
#include <QProcess>
#include <QThread>
#include <QString>
#include <QCursor>
#include <QDebug>
#include <QFrame>
#include <QFont>
#include <QDir>


namespace xin {

enum Language {
    simplifiedChinese,
    English
};
//以下内容注意自修改
static const QString qrcName = ":/files/";
static const int nameSize = qrcName.size();
static const QString UninstallName = "xiaoxin卸载.exe";
static const QString iconName = "xin.ico";
static const QString exeName = "XiaoXin";
static const QString executableExeName = "Xin.exe";
static const QString versions = "1.0.0";
}

#endif // MAIN_H
