#include "introduction.h"
#include "ui_introduction.h"
#include "main.h"

Introduction::Introduction(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Introduction),
    licenseCheck(nullptr)
{
    ui->setupUi(this);

    this->setWindowIcon(QIcon(":/resourceFile/xin.ico"));
    this->setWindowTitle(tr("%1 %2 安装").arg(xin::exeName).arg(xin::versions));
    ui->labelWallpaper->setScaledContents(true);
    ui->labelWallpaper->setPixmap(QPixmap(":/resourceFile/123.png"));

//    ui->textEditTitle->setFrameShape(QFrame::Shape::NoFrame); //设置无边框
//    ui->textEditTitle->setReadOnly(true);
//    ui->textEditTitle->setCursor(QCursor(Qt::CursorShape::ArrowCursor));
    ui->textEditcontent->setFrameShape(QFrame::Shape::NoFrame); //设置无边框
    ui->textEditcontent->setReadOnly(true);
    ui->textEditcontent->setCursor(QCursor(Qt::CursorShape::ArrowCursor)); //如果不显示该鼠标!一定属性里面在点一下!

    ui->widget->move(this->x() - 9, -9);


    ui->labelTitleIntroduction->setFont(QFont("Times", 10, QFont::Bold));
    ui->labelTitleIntroduction->setWordWrap(true);
    ui->labelTitleIntroduction->setText(tr("\n 欢迎使用\"%1 %2\"安装向导").arg(xin::exeName).arg(xin::versions));
    ui->textEditcontent->setText(tr("\n  这个向导将指引您完成 %1 %2 的安装。\n\n  建议在继续之前先退出其他运行的程序。\n\n  单击 [下一步] 继续. \n\n  或者单击 [取消] 退出本安装程序。")
                                 .arg(xin::exeName).arg(xin::versions));
    ui->pushButtonNext->setText(tr("下一步(N) >"));
    ui->pushButtonNext->setShortcut(Qt::Key_N); //设置快捷键
    ui->pushButtonCancel->setText(tr("取消(C)"));
    ui->pushButtonCancel->setShortcut(Qt::Key_C);
    connect(ui->pushButtonNext, SIGNAL(clicked(bool)), this, SLOT(buttonNext()));
    connect(ui->pushButtonCancel, SIGNAL(clicked(bool)), this, SLOT(buttonCancel()));
}

Introduction::~Introduction()
{
    delete ui;
}

void Introduction::closeEvent(QCloseEvent *event)
{
    QMessageBox* box = new QMessageBox(QMessageBox::Question, tr("%1 %2的安装").arg(xin::exeName).arg(xin::versions) ,
                                      tr("你确定要退出%1 %2 的安装吗?").arg(xin::exeName).arg(xin::versions),
                                       QMessageBox::Yes | QMessageBox::No);

    box->button(QMessageBox::Yes)->setText(tr("是"));
    box->button(QMessageBox::No)->setText(tr("否"));
    box->button(QMessageBox::No)->setFocus(); //设置焦点
    //此move会有变动
    box->move(this->x() + (((this->width() - 310) / 2)), this->y() + ((this->height() - 120) / 2));
    int ret = box->exec();
    if (ret != QMessageBox::Yes) {
        event->ignore();
    }
}

void Introduction::showIntroduction(QPoint point)
{
    this->move(point);
    this->show();
}

void Introduction::buttonNext()
{
    if (licenseCheck == nullptr) {
        licenseCheck = new LicenseCheck();
        QDesktopWidget* desktop = QApplication::desktop(); // =qApp->desktop();也可以
        licenseCheck->move((desktop->width() - licenseCheck->width())/2, (desktop->height() - licenseCheck->height())/2);
        connect(licenseCheck, SIGNAL(signalShowIntroduction(QPoint)), this, SLOT(showIntroduction(QPoint)));
    }
    QPoint point = this->frameGeometry().topLeft();
    licenseCheck->setMoveWindows(point);
    licenseCheck->show();
    this->hide();
}

void Introduction::buttonCancel()
{
    QMessageBox* box = new QMessageBox(QMessageBox::Question, tr("%1 %2的安装").arg(xin::exeName).arg(xin::versions) ,
                                      tr("你确定要退出%1 %2 的安装吗?").arg(xin::exeName).arg(xin::versions),
                                       QMessageBox::Yes | QMessageBox::No);
    box->button(QMessageBox::Yes)->setText(tr("是"));
    box->button(QMessageBox::No)->setText(tr("否"));
    box->button(QMessageBox::No)->setFocus(); //设置焦点
    //此move会有变动
    box->move(this->x() + (((this->width() - 310) / 2)), this->y() + ((this->height() - 120) / 2));
    int ret = box->exec();
    if (ret == QMessageBox::Yes) {
        exit(1);
    }
}
